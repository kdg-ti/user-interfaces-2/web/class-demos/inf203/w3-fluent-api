// let objecten = "beans";
//
// function showMessage(number) {
//   console.log(number + " " + objecten);
// }

//showMessage(5); // 5 beans

let things = "crows";
function showMessage(number) {console.log(number + " " + things);}

function makeCounter() {
  let count = 2;
  
  return function () {
    return count++;
  };
}

const counter = makeCounter();

// dit werkt niet: geen toegang tot count meer!!
//showMessage(count)
showMessage(counter()); // 2 crows
showMessage(counter()); // 3 crows
showMessage(counter()); // 4 crows

// multiplier voorbeeld
// function multiplier (factor){
//   function doIt(number){
//     return number * factor;
//   }
//   return doIt;
// }

// kortere schrijfwijze: lambda
function multiplier (factor){
  return  number =>  number * factor;
}

const double = multiplier (2);
const triple = multiplier (3);

console.log(double(76));
console.log(triple(5));


