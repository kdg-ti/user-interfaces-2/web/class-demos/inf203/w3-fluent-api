import {data} from "./db.js";

const contents = document.getElementById("contents");
// filter functie
//contents.innerHTML=JSON.stringify(getMultiTaskers(data.persons,p => p.gender === "M"));
function getMultiTaskers(persons,test){
  let result = [];
  for (let person of persons){
    //   if (person.gender === "F") {µ
  
    // replacing if condition with test function as a parameter
    if (test(person)) {
      result.push(person);
    }
  }
  return result;
}
// filter stream: fluent array API
// contents.innerHTML=JSON.stringify(data.persons
//   .filter(p => p.gender === "F")
//   .map(p => `${p.firstName}  ${p.lastName} ${p.yearsService}`));

// met reduce
contents.innerHTML=data.persons
  .filter(p => p.gender === "F")
 // .map(p => `${p.firstName}  ${p.lastName} ${p.yearsService}`)
  .reduce(
    (result,p) => `${result} <tr><td> ${p.firstName}  ${p.lastName}</td><td> ${p.yearsService}</td</tr>`,
    "<table>") + "</table>";


